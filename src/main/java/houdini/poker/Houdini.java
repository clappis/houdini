package houdini.poker;

import houdini.poker.action.Action;
import houdini.poker.hand.PokerHand;

public abstract class Houdini<T extends PokerHand>  {

	public abstract Action play(T hand);
	
}
