package houdini.poker.decision;

import houdini.poker.action.Action;
import houdini.poker.hand.texas.TexasHand;

public class HoudiniTexasDecisionTurn extends HoudiniTexasDecision {

	public HoudiniTexasDecisionTurn(TexasHand hand) {
		super(hand);
	}

	@Override
	public Action decideMove() {
		return null;
	}

}
