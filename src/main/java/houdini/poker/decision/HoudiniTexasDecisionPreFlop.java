package houdini.poker.decision;

import houdini.poker.action.Action;
import houdini.poker.action.Call;
import houdini.poker.action.Fold;
import houdini.poker.action.Raise;
import houdini.poker.hand.texas.TexasHand;
import houdini.poker.hand.texas.TexasHandCards;
import houdini.poker.position.Position;
import houdini.poker.range.HoudiniTexasRange3BetPreFlop;
import houdini.poker.range.HoudiniTexasRange3BetPreFlopFactory;

import java.util.List;

public class HoudiniTexasDecisionPreFlop extends HoudiniTexasDecision {

	private final HoudiniTexasRange3BetPreFlop range3Bet;
	
	public HoudiniTexasDecisionPreFlop(TexasHand hand) {
		super(hand);
		range3Bet = HoudiniTexasRange3BetPreFlopFactory.getInstance(null);
	}

	@Override
	public Action decideMove() {
		if (hasVoluntaryPlayerInPot()) {
			if (range3Bet.contains(null, null))
				return new Raise();
			if (rangeForCallIn(getHoudiniPosition()).contains(getHoudiniCards()))
				return new Call();
				
			return new Fold();
		}
		
		if (rangeForOpenIn(getHoudiniPosition()).contains(getHoudiniCards()))
			return new Raise();
		
		return new Fold();
	}

	private List<TexasHandCards> rangeForOpenIn(Position houdiniPosition) {
		return null;
	}
	
	private List<TexasHandCards> rangeForCallIn(Position houdiniPosition) {
		return null;
	}

	private boolean hasVoluntaryPlayerInPot() {
		return false;
	}

}
