package houdini.poker.decision;

import houdini.poker.action.Action;
import houdini.poker.hand.texas.TexasHand;

public class HoudiniTexasDecisionFlop extends HoudiniTexasDecision {

	public HoudiniTexasDecisionFlop(TexasHand hand) {
		super(hand);
	}

	@Override
	public Action decideMove() {
		return null;
	}

}
