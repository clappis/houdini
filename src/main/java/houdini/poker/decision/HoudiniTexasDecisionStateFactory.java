package houdini.poker.decision;

import houdini.poker.hand.texas.TexasHand;

public class HoudiniTexasDecisionStateFactory {

	public static HoudiniTexasDecision getInstance(TexasHand hand) {
		if (hand.getFlop() == null)
			return new HoudiniTexasDecisionPreFlop(hand);
		if (hand.getTurn() == null)
			return new HoudiniTexasDecisionFlop(hand);
		if (hand.getRiver() == null)
			return new HoudiniTexasDecisionTurn(hand);
		
		return new HoudiniTexasDecisionRiver(hand);
	}

}
