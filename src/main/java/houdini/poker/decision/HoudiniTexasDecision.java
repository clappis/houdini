package houdini.poker.decision;

import houdini.poker.HoudiniTexas;
import houdini.poker.action.Action;
import houdini.poker.hand.texas.TexasHand;
import houdini.poker.position.Position;

public abstract class HoudiniTexasDecision {

	private TexasHand hand;

	public HoudiniTexasDecision(TexasHand hand) {
		this.hand = hand;
	}
	
	public abstract Action decideMove();

	protected Position getHoudiniPosition() {
		return null;
	}
	
	protected HoudiniTexas getHoudini() {
		return null;
	}
	
	protected Position getHoudiniCards() {
		return null;
	}
	
	public TexasHand getHand() {
		return hand;
	}
	
}
