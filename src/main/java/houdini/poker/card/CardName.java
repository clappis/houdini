package houdini.poker.card;

public enum CardName {

	A("A"),
	K("K"),
	Q("Q"),
	J("J"),
	T("T"),
	NINE("9"),
	EIGHT("8"),
	SEVEN("7"),
	SIX("6"),
	FIVE("5"),
	FOUR("4"),
	THREE("3"),
	TWO("2");
	
	CardName(String name){
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	private String name;
}
