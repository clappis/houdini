package houdini.poker.card;

public class Card {
	
	private Nype nype;
	private CardName cardName;

	public Card(Nype nype, CardName cardName) {
		this.nype = nype;
		this.cardName = cardName;
	}

	public Nype getNype() {
		return nype;
	}

	public void setNype(Nype nype) {
		this.nype = nype;
	}

	public CardName getCardName() {
		return cardName;
	}
}
