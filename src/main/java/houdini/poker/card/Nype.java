package houdini.poker.card;

public enum Nype {
	CLUBS("c"),
	DIAMONDS("d"),
	HURTS("h"),
	SPACES("s");
	
	private String nype;

	Nype(String nype){
		this.nype = nype;
	}

	public String getNype(){
		return nype;
	}
}
