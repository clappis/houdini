package houdini.poker;

import houdini.poker.action.Action;
import houdini.poker.decision.HoudiniTexasDecision;
import houdini.poker.decision.HoudiniTexasDecisionStateFactory;
import houdini.poker.hand.texas.TexasHand;

public class HoudiniTexas extends Houdini<TexasHand> {

	public Action play(TexasHand hand) {
		HoudiniTexasDecision houdini = HoudiniTexasDecisionStateFactory.getInstance(hand);
		return houdini.decideMove();
	}


}
