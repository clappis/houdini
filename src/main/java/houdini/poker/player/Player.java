package houdini.poker.player;

import houdini.poker.hand.PokerHandCards;
import houdini.poker.position.Position;

public class Player {

	private Position position;
	private Double stack;
	private String name;
	private PokerHandCards cards;

	public Player(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Double getStack() {
		return stack;
	}

	public void setStack(Double stack) {
		this.stack = stack;
	}

	public PokerHandCards getCards() {
		return cards;
	}

	public void setCards(PokerHandCards cards) {
		this.cards = cards;
	}
}
