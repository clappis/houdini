package houdini.poker.hand;
import houdini.poker.hand.state.Flop;
import houdini.poker.hand.state.PreFlop;
import houdini.poker.hand.state.River;
import houdini.poker.hand.state.Turn;
import houdini.poker.player.Player;

import java.util.List;

public abstract class ClassicalHand<T extends PokerHandCards> implements PokerHand {

	private PreFlop preFlop;
	private Flop flop;
	private Turn turn;
	private River river;
	private List<Player> players;
	
	public PreFlop getPreFlop() {
		return preFlop;
	}
	
	public void setPreFlop(PreFlop preFlop) {
		this.preFlop = preFlop;
	}
	
	public Flop getFlop() {
		return flop;
	}
	
	public void setFlop(Flop flop) {
		this.flop = flop;
	}
	
	public Turn getTurn() {
		return turn;
	}
	
	public void setTurn(Turn turn) {
		this.turn = turn;
	}
	
	public River getRiver() {
		return river;
	}
	
	public void setRiver(River river) {
		this.river = river;
	}
	
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	
	public List<Player> getPlayers() {
		return players;
	}
}
