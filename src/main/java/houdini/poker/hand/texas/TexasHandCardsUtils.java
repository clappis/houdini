package houdini.poker.hand.texas;

import static houdini.poker.card.CardName.A;
import static houdini.poker.card.CardName.EIGHT;
import static houdini.poker.card.CardName.FIVE;
import static houdini.poker.card.CardName.FOUR;
import static houdini.poker.card.CardName.J;
import static houdini.poker.card.CardName.K;
import static houdini.poker.card.CardName.NINE;
import static houdini.poker.card.CardName.Q;
import static houdini.poker.card.CardName.SEVEN;
import static houdini.poker.card.CardName.SIX;
import static houdini.poker.card.CardName.T;
import static houdini.poker.card.CardName.THREE;
import static houdini.poker.card.CardName.TWO;
import static houdini.poker.card.Nype.CLUBS;
import static houdini.poker.card.Nype.DIAMONDS;
import static houdini.poker.card.Nype.HURTS;
import static houdini.poker.card.Nype.SPACES;
import static java.util.Arrays.asList;
import houdini.poker.card.Card;
import houdini.poker.card.CardName;

import java.util.ArrayList;
import java.util.List;

public class TexasHandCardsUtils {

	public static TexasHandCards AsKs = new TexasHandCards(new Card(SPACES, A), new Card(SPACES, K)); 
	public static TexasHandCards AhKh = new TexasHandCards(new Card(HURTS, K), new Card(HURTS, K)); 
	public static TexasHandCards AdKd = new TexasHandCards(new Card(DIAMONDS, K), new Card(DIAMONDS, K)); 
	public static TexasHandCards AcKc = new TexasHandCards(new Card(CLUBS, K), new Card(CLUBS, K));
	
	public static List<TexasHandCards> AKs = asList(AsKs, AhKh, AdKd, AcKc);

	public static List<TexasHandCards> fullRange() {
		ArrayList<TexasHandCards> fullRange = new ArrayList<TexasHandCards>();
		fullRange.addAll(pairsOf(A));
		fullRange.addAll(pairsOf(K));
		fullRange.addAll(pairsOf(Q));
		fullRange.addAll(suitedHandTo(A, K));
		fullRange.addAll(offHandTo(A, K));
		fullRange.addAll(pairsOf(J));
		fullRange.addAll(suitedHandTo(A, Q));
		fullRange.addAll(offHandTo(A, Q));
		fullRange.addAll(suitedHandTo(K, Q));
		
		fullRange.addAll(pairsOf(T));
		fullRange.addAll(suitedHandTo(A, J));
		fullRange.addAll(offHandTo(K, Q));
		fullRange.addAll(pairsOf(NINE));
		fullRange.addAll(pairsOf(EIGHT));
		
		fullRange.addAll(suitedHandTo(A, T));
		fullRange.addAll(offHandTo(A, J));
		
		fullRange.addAll(pairsOf(SEVEN));
		fullRange.addAll(pairsOf(SIX));
		fullRange.addAll(pairsOf(FIVE));
		
		fullRange.addAll(suitedHandTo(K, J));
		fullRange.addAll(offHandTo(A, T));
		fullRange.addAll(offHandTo(K, J));
		fullRange.addAll(suitedHandTo(K, T));
		
		fullRange.addAll(pairsOf(FOUR));
		fullRange.addAll(pairsOf(THREE));
		fullRange.addAll(pairsOf(TWO));
		
		fullRange.addAll(suitedHandTo(Q, J));
		fullRange.addAll(offHandTo(K, T));
		fullRange.addAll(offHandTo(Q, J));
		
		fullRange.addAll(suitedHandTo(A, NINE));
		fullRange.addAll(suitedHandTo(A, EIGHT));
		fullRange.addAll(suitedHandTo(A, SEVEN));
		fullRange.addAll(suitedHandTo(A, SIX));
		fullRange.addAll(suitedHandTo(A, FIVE));
		fullRange.addAll(suitedHandTo(A, FOUR));
		fullRange.addAll(suitedHandTo(A, THREE));
		fullRange.addAll(suitedHandTo(A, TWO));
		
		fullRange.addAll(offHandTo(A, NINE));
		fullRange.addAll(offHandTo(A, EIGHT));
		fullRange.addAll(offHandTo(A, SEVEN));
		fullRange.addAll(offHandTo(A, SIX));
		fullRange.addAll(offHandTo(A, FIVE));
		fullRange.addAll(offHandTo(A, FOUR));
		fullRange.addAll(offHandTo(A, THREE));
		fullRange.addAll(offHandTo(A, TWO));
		
		fullRange.addAll(suitedHandTo(Q, T));
		fullRange.addAll(offHandTo(Q, T));
		
		
		fullRange.addAll(suitedHandTo(K, NINE));
		fullRange.addAll(suitedHandTo(K, EIGHT));
		fullRange.addAll(suitedHandTo(K, SEVEN));
		fullRange.addAll(suitedHandTo(K, SIX));
		fullRange.addAll(suitedHandTo(K, FIVE));
		fullRange.addAll(suitedHandTo(K, FOUR));
		fullRange.addAll(suitedHandTo(K, THREE));
		fullRange.addAll(suitedHandTo(K, TWO));
		
		fullRange.addAll(offHandTo(K, NINE));
		fullRange.addAll(offHandTo(K, EIGHT));
		fullRange.addAll(offHandTo(K, SEVEN));
		fullRange.addAll(offHandTo(K, SIX));
		fullRange.addAll(offHandTo(K, FIVE));
		fullRange.addAll(offHandTo(K, FOUR));
		fullRange.addAll(offHandTo(K, THREE));
		fullRange.addAll(offHandTo(K, TWO));
		
		fullRange.addAll(suitedHandTo(Q, NINE));
		fullRange.addAll(suitedHandTo(Q, EIGHT));
		fullRange.addAll(suitedHandTo(Q, SEVEN));
		fullRange.addAll(suitedHandTo(Q, SIX));
		fullRange.addAll(suitedHandTo(Q, FIVE));
		fullRange.addAll(suitedHandTo(Q, FOUR));
		fullRange.addAll(suitedHandTo(Q, THREE));
		fullRange.addAll(suitedHandTo(Q, TWO));
		
		fullRange.addAll(offHandTo(Q, NINE));
		fullRange.addAll(offHandTo(Q, EIGHT));
		fullRange.addAll(offHandTo(Q, SEVEN));
		fullRange.addAll(offHandTo(Q, SIX));
		fullRange.addAll(offHandTo(Q, FIVE));
		fullRange.addAll(offHandTo(Q, FOUR));
		fullRange.addAll(offHandTo(Q, THREE));
		fullRange.addAll(offHandTo(Q, TWO));
		
		fullRange.addAll(suitedHandTo(J, T));
		fullRange.addAll(suitedHandTo(J, NINE));
		fullRange.addAll(suitedHandTo(J, EIGHT));
		fullRange.addAll(suitedHandTo(J, SEVEN));
		fullRange.addAll(suitedHandTo(J, SIX));
		fullRange.addAll(suitedHandTo(J, FIVE));
		fullRange.addAll(suitedHandTo(J, FOUR));
		fullRange.addAll(suitedHandTo(J, THREE));
		fullRange.addAll(suitedHandTo(J, TWO));
		
		fullRange.addAll(offHandTo(J, T));
		fullRange.addAll(offHandTo(J, NINE));
		fullRange.addAll(offHandTo(J, EIGHT));
		fullRange.addAll(offHandTo(J, SEVEN));
		fullRange.addAll(offHandTo(J, SIX));
		fullRange.addAll(offHandTo(J, FIVE));
		fullRange.addAll(offHandTo(J, FOUR));
		fullRange.addAll(offHandTo(J, THREE));
		fullRange.addAll(offHandTo(J, TWO));
		
		fullRange.addAll(suitedHandTo(T, NINE));
		fullRange.addAll(suitedHandTo(T, EIGHT));
		fullRange.addAll(suitedHandTo(T, SEVEN));
		fullRange.addAll(suitedHandTo(T, SIX));
		fullRange.addAll(suitedHandTo(T, FIVE));
		fullRange.addAll(suitedHandTo(T, FOUR));
		fullRange.addAll(suitedHandTo(T, THREE));
		fullRange.addAll(suitedHandTo(T, TWO));
		
		fullRange.addAll(offHandTo(T, NINE));
		fullRange.addAll(offHandTo(T, EIGHT));
		fullRange.addAll(offHandTo(T, SEVEN));
		fullRange.addAll(offHandTo(T, SIX));
		fullRange.addAll(offHandTo(T, FIVE));
		fullRange.addAll(offHandTo(T, FOUR));
		fullRange.addAll(offHandTo(T, THREE));
		fullRange.addAll(offHandTo(T, TWO));
		
		fullRange.addAll(suitedHandTo(NINE, EIGHT));
		fullRange.addAll(suitedHandTo(NINE, SEVEN));
		fullRange.addAll(suitedHandTo(NINE, SIX));
		fullRange.addAll(suitedHandTo(NINE, FIVE));
		fullRange.addAll(suitedHandTo(NINE, FOUR));
		fullRange.addAll(suitedHandTo(NINE, THREE));
		fullRange.addAll(suitedHandTo(NINE, TWO));
		
		fullRange.addAll(offHandTo(NINE, EIGHT));
		fullRange.addAll(offHandTo(NINE, SEVEN));
		fullRange.addAll(offHandTo(NINE, SIX));
		fullRange.addAll(offHandTo(NINE, FIVE));
		fullRange.addAll(offHandTo(NINE, FOUR));
		fullRange.addAll(offHandTo(NINE, THREE));
		fullRange.addAll(offHandTo(NINE, TWO));
		
		fullRange.addAll(suitedHandTo(EIGHT, SEVEN));
		fullRange.addAll(suitedHandTo(EIGHT, SIX));
		fullRange.addAll(suitedHandTo(EIGHT, FIVE));
		fullRange.addAll(suitedHandTo(EIGHT, FOUR));
		fullRange.addAll(suitedHandTo(EIGHT, THREE));
		fullRange.addAll(suitedHandTo(EIGHT, TWO));
		
		fullRange.addAll(offHandTo(EIGHT, SEVEN));
		fullRange.addAll(offHandTo(EIGHT, SIX));
		fullRange.addAll(offHandTo(EIGHT, FIVE));
		fullRange.addAll(offHandTo(EIGHT, FOUR));
		fullRange.addAll(offHandTo(EIGHT, THREE));
		fullRange.addAll(offHandTo(EIGHT, TWO));
		
		fullRange.addAll(suitedHandTo(SEVEN, SIX));
		fullRange.addAll(suitedHandTo(SEVEN, FIVE));
		fullRange.addAll(suitedHandTo(SEVEN, FOUR));
		fullRange.addAll(suitedHandTo(SEVEN, THREE));
		fullRange.addAll(suitedHandTo(SEVEN, TWO));
		
		fullRange.addAll(offHandTo(SEVEN, SIX));
		fullRange.addAll(offHandTo(SEVEN, FIVE));
		fullRange.addAll(offHandTo(SEVEN, FOUR));
		fullRange.addAll(offHandTo(SEVEN, THREE));
		fullRange.addAll(offHandTo(SEVEN, TWO));

		fullRange.addAll(suitedHandTo(SIX, FIVE));
		fullRange.addAll(suitedHandTo(SIX, FOUR));
		fullRange.addAll(suitedHandTo(SIX, THREE));
		fullRange.addAll(suitedHandTo(SIX, TWO));
		
		fullRange.addAll(offHandTo(SIX, FIVE));
		fullRange.addAll(offHandTo(SIX, FOUR));
		fullRange.addAll(offHandTo(SIX, THREE));
		fullRange.addAll(offHandTo(SIX, TWO));
		
		fullRange.addAll(suitedHandTo(FIVE, FOUR));
		fullRange.addAll(suitedHandTo(FIVE, THREE));
		fullRange.addAll(suitedHandTo(FIVE, TWO));
		
		fullRange.addAll(offHandTo(FIVE, FOUR));
		fullRange.addAll(offHandTo(FIVE, THREE));
		fullRange.addAll(offHandTo(FIVE, TWO));
		
		fullRange.addAll(suitedHandTo(FOUR, THREE));
		fullRange.addAll(suitedHandTo(FOUR, TWO));
		
		fullRange.addAll(offHandTo(FOUR, THREE));
		fullRange.addAll(offHandTo(FOUR, TWO));
		
		fullRange.addAll(suitedHandTo(THREE, TWO));
		
		fullRange.addAll(offHandTo(THREE, TWO));
		return fullRange;
	} 
	
	public static List<TexasHandCards> pairsOf(CardName cardName){
		ArrayList<TexasHandCards> pairs = new ArrayList<TexasHandCards>();
		pairs.add(new TexasHandCards(new Card(SPACES, cardName), new Card(HURTS, cardName)));
		pairs.add(new TexasHandCards(new Card(SPACES, cardName), new Card(DIAMONDS, cardName)));
		pairs.add(new TexasHandCards(new Card(SPACES, cardName), new Card(CLUBS, cardName)));
		pairs.add(new TexasHandCards(new Card(DIAMONDS, cardName), new Card(HURTS, cardName)));
		pairs.add(new TexasHandCards(new Card(DIAMONDS, cardName), new Card(CLUBS, cardName)));
		pairs.add(new TexasHandCards(new Card(CLUBS, cardName), new Card(HURTS, cardName)));
		return pairs;
	}
	
	public static List<TexasHandCards> suitedHandTo(CardName cardNameOne, CardName cardNameTwo){
		ArrayList<TexasHandCards> suitedHands = new ArrayList<TexasHandCards>();
		suitedHands.add(new TexasHandCards(new Card(SPACES, cardNameOne), new Card(SPACES, cardNameTwo)));  
		suitedHands.add(new TexasHandCards(new Card(HURTS, cardNameOne), new Card(HURTS, cardNameTwo)));    
		suitedHands.add(new TexasHandCards(new Card(DIAMONDS, cardNameOne), new Card(DIAMONDS, cardNameTwo)));
		suitedHands.add(new TexasHandCards(new Card(CLUBS, cardNameOne), new Card(CLUBS, cardNameTwo)));    
		return suitedHands;
	}
	
	public static List<TexasHandCards> offHandTo(CardName cardNameOne, CardName cardNameTwo){
		ArrayList<TexasHandCards> offHands = new ArrayList<TexasHandCards>();
		offHands.add(new TexasHandCards(new Card(SPACES, cardNameOne), new Card(HURTS, cardNameTwo)));  
		offHands.add(new TexasHandCards(new Card(SPACES, cardNameOne), new Card(DIAMONDS, cardNameTwo)));  
		offHands.add(new TexasHandCards(new Card(SPACES, cardNameOne), new Card(CLUBS, cardNameTwo)));
		
		offHands.add(new TexasHandCards(new Card(HURTS, cardNameOne), new Card(SPACES, cardNameTwo)));  
		offHands.add(new TexasHandCards(new Card(HURTS, cardNameOne), new Card(DIAMONDS, cardNameTwo)));  
		offHands.add(new TexasHandCards(new Card(HURTS, cardNameOne), new Card(CLUBS, cardNameTwo)));
		
		offHands.add(new TexasHandCards(new Card(CLUBS, cardNameOne), new Card(SPACES, cardNameTwo)));  
		offHands.add(new TexasHandCards(new Card(CLUBS, cardNameOne), new Card(DIAMONDS, cardNameTwo)));  
		offHands.add(new TexasHandCards(new Card(CLUBS, cardNameOne), new Card(HURTS, cardNameTwo)));
		
		offHands.add(new TexasHandCards(new Card(DIAMONDS, cardNameOne), new Card(SPACES, cardNameTwo)));  
		offHands.add(new TexasHandCards(new Card(DIAMONDS, cardNameOne), new Card(CLUBS, cardNameTwo)));  
		offHands.add(new TexasHandCards(new Card(DIAMONDS, cardNameOne), new Card(HURTS, cardNameTwo)));
		
		return offHands;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
