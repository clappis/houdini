package houdini.poker.hand.texas;

import houdini.poker.hand.state.Flop;
import houdini.poker.hand.state.PreFlop;
import houdini.poker.hand.state.River;
import houdini.poker.hand.state.Turn;
import houdini.poker.player.Player;

import java.util.List;

public class TexasHandBuilder {

	private TexasHand texasHand;

	public TexasHandBuilder(){
		texasHand = new TexasHand();
	}
	
	public TexasHand build() {
		return texasHand;
	}

	public TexasHandBuilder preFlop(PreFlop preFlop) {
		texasHand.setPreFlop(preFlop);
		return this;
	}

	public TexasHandBuilder flop(Flop flop) {
		texasHand.setFlop(flop);
		return this;
	}

	public TexasHandBuilder turn(Turn turn) {
		texasHand.setTurn(turn);
		return this;
	}

	public TexasHandBuilder river(River river) {
		texasHand.setRiver(river);
		return this;
	}

	public TexasHandBuilder players(List<Player> players) {
		texasHand.setPlayers(players);
		return this;
	}
}
