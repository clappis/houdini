package houdini.poker.hand.texas;

import houdini.poker.card.Card;
import houdini.poker.hand.PokerHandCards;

public class TexasHandCards implements PokerHandCards {

	private Card cardOne;
	private Card cardTwo;

	public TexasHandCards(Card cardOne, Card cardTwo) {
		this.cardOne = cardOne;
		this.cardTwo = cardTwo;
	}
	
	public Card getCardTwo() {
		return cardTwo;
	}

	public Card getCardOne() {
		return cardOne;
	}
	
	@Override
	public String toString() {
		return cardOne.getCardName().name() + 
			   cardOne.getNype().getNype() +
			   cardTwo.getCardName().name() +
			   cardTwo.getNype().getNype();
	}
}
