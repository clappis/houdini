package houdini.poker.hand.state;

import houdini.poker.card.Card;

public class Flop extends StateHand {

	private Card firstCard;
	private Card secondCard;
	private Card thirdCard;

	protected Flop() {
	}
	
	public Card getFirstCard() {
		return firstCard;
	}
	
	public Card getSecondCard() {
		return secondCard;
	}
	
	public Card getThirdCard() {
		return thirdCard;
	}

	public void setFirstCard(Card firstCard) {
		this.firstCard = firstCard;
	}

	public void setSecondCard(Card secondCard) {
		this.secondCard = secondCard;
	}

	public void setThirdCard(Card thirdCard) {
		this.thirdCard = thirdCard;
	}
}
