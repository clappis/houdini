package houdini.poker.hand.state;

import houdini.poker.action.Action;
import houdini.poker.card.Card;
import houdini.poker.player.Player;

import java.util.Map;

public class RiverBuilder {
	
	private River river;

	public RiverBuilder(){
		river = new River();
	}

	public River build() {
		return river;
	}

	public RiverBuilder card(Card card) {
		river.setCard(card);
		return this;
	}

	public RiverBuilder addPlayerAction(Map<Player, Action> playerAction) {
		river.addPlayerAction(playerAction);
		return this;
	}

}
