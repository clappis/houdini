package houdini.poker.hand.state;

import houdini.poker.action.Action;
import houdini.poker.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class StateHand {

	private List<Map<Player, Action>> playersActions = new ArrayList<Map<Player, Action>>();

	public List<Map<Player, Action>> getPlayersActions() {
		return playersActions;
	}
	
	public void addPlayerAction(Map<Player, Action> playerAction) {
		playersActions.add(playerAction);
	}
}
