package houdini.poker.hand.state;

import houdini.poker.action.Action;
import houdini.poker.card.Card;
import houdini.poker.player.Player;

import java.util.Map;

public class FlopBuilder  {
	
	private Flop flop;

	public FlopBuilder(){
		flop = new Flop();
	}
	
	public FlopBuilder firstCard(Card firstCard) {
		flop.setFirstCard(firstCard);
		return this;
	}

	public FlopBuilder secondCard(Card secondCard) {
		flop.setSecondCard(secondCard);
		return this;
	}

	public FlopBuilder thirdCard(Card thirdCard) {
		flop.setThirdCard(thirdCard);
		return this;
	}
	
	public Flop build(){
		return flop;
	}
	
	public FlopBuilder addPlayerAction(Map<Player, Action> playerAction) {
		flop.addPlayerAction(playerAction);
		return this;
	}

}
