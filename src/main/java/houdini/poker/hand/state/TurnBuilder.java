package houdini.poker.hand.state;

import houdini.poker.action.Action;
import houdini.poker.card.Card;
import houdini.poker.player.Player;

import java.util.Map;

public class TurnBuilder {

	private Turn turn;

	public TurnBuilder(){
		turn = new Turn();
	}
	
	public Turn build() {
		return turn;
	}

	public TurnBuilder addPlayerAction(Map<Player, Action> playerAction) {
		turn.addPlayerAction(playerAction);
		return this;
	}

	public TurnBuilder card(Card card) {
		turn.setCard(card);
		return this;
	}

}
