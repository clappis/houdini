package houdini.poker.hand.state;

import houdini.poker.card.Card;

public class Turn extends StateHand {

	private Card card;

	public Card getCard() {
		return card;
	}

	protected void setCard(Card card) {
		this.card = card;
	}
	
}
