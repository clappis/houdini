package houdini.poker.hand.state;

import houdini.poker.card.Card;

public class River extends StateHand {

	private Card card;

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

}
