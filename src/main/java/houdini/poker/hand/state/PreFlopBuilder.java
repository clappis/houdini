package houdini.poker.hand.state;

import houdini.poker.action.Action;
import houdini.poker.player.Player;

import java.util.Map;

public class PreFlopBuilder {

	private PreFlop preFlop;

	public PreFlopBuilder() {
		preFlop = new PreFlop();
	}
	
	public PreFlopBuilder addPlayerAction(Map<Player, Action> playerAction) {
		preFlop.addPlayerAction(playerAction);
		return this;
	}
	
	public PreFlop build(){
		return preFlop;
	}

}
