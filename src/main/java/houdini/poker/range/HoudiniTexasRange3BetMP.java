package houdini.poker.range;


public class HoudiniTexasRange3BetMP extends HoudiniTexasRange3BetPreFlop {

	@Override
	protected int percentualRange() {
		return 14;
	}

}
