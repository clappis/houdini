package houdini.poker.range;

import houdini.poker.position.Position;
import houdini.poker.position.UTG;

public class HoudiniTexasRange3BetPreFlopFactory {

	public static HoudiniTexasRange3BetPreFlop getInstance(Position position){
		if (position instanceof UTG)
			return new HoudiniTexasRange3BetUTG();
		return null;
	}
	
}
