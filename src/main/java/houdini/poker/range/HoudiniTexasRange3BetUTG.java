package houdini.poker.range;


public class HoudiniTexasRange3BetUTG extends HoudiniTexasRange3BetPreFlop {

	@Override
	protected int percentualRange() {
		return 14;
	}

}
