package houdini.poker.range;

import static houdini.poker.hand.texas.TexasHandCardsUtils.fullRange;
import houdini.poker.hand.texas.TexasHandCards;
import houdini.poker.player.Player;

import java.util.List;

public abstract class HoudiniTexasRange3BetPreFlop {

	public List<TexasHandCards> fullRange = fullRange();
	
	public boolean contains(TexasHandCards card, List<Player> players) {
		return fullRange.subList(0, getLimiteRange(players)).contains(card);
	}

	private int getLimiteRange(List<Player> players) {
		double percentualAcumulado = 0;
		if (players.size() == 1) {
			//getStateFoldTo3bet;
			//if fold-to-3bet > 80 %
			//percentualAcumulado = 10%;
		}
		
		return fullRange.size() * percentualRange();
	}

	protected abstract int percentualRange();
}
