package houdini.poker.hand.texas;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import houdini.poker.hand.state.Flop;
import houdini.poker.hand.state.PreFlop;
import houdini.poker.hand.state.River;
import houdini.poker.hand.state.Turn;
import houdini.poker.player.Player;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TexasHandBuilderTest {

	@Mock
	private PreFlop preFlop;
	
	@Mock
	private Flop flop;
	
	@Mock
	private Turn turn;

	@Mock
	private River river;
	
	@Mock
	private List<Player> players;
	
	@Test
	public void buildTexasHand(){
		assertThat(new TexasHandBuilder().build(), instanceOf(TexasHand.class));
	}
	
	@Test
	public void buildTexasHandWithPreFlop(){
		TexasHand texasHand = new TexasHandBuilder().preFlop(preFlop).build();
		assertThat(texasHand.getPreFlop(), is(preFlop));
	}
	
	@Test
	public void buildTexasHandWithFlop(){
		TexasHand texasHand = new TexasHandBuilder().flop(flop).build();
		assertThat(texasHand.getFlop(), is(flop));
	}
	
	@Test
	public void buildTexasHandWithTurn(){
		TexasHand texasHand = new TexasHandBuilder().turn(turn).build();
		assertThat(texasHand.getTurn(), is(turn));
	}
	
	@Test
	public void buildTexasHandWithRiver(){
		TexasHand texasHand = new TexasHandBuilder().river(river).build();
		assertThat(texasHand.getRiver(), is(river));
	}
	
	@Test
	public void buildTexasHandWithPlayersHand(){
		TexasHand texasHand = new TexasHandBuilder().players(players).build();
		assertThat(texasHand.getPlayers(), is(players));
	}
	
}
