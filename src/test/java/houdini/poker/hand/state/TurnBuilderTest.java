package houdini.poker.hand.state;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import houdini.poker.action.Action;
import houdini.poker.card.Card;
import houdini.poker.player.Player;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TurnBuilderTest {

	@Mock
	private Map<Player, Action> playerAction;
	
	@Mock
	private Card card;
	
	@Test
	public void buildTurn(){
		assertThat(new TurnBuilder().build(), instanceOf(Turn.class));
	}
	
	@Test
	public void buildTurnWithCard(){
		Turn turn = new TurnBuilder().card(card).build();
		assertThat(turn.getCard(), is(card));
	}
	
	@Test
	public void buildTurnWithPlayerAction(){
		Turn turn = new TurnBuilder().addPlayerAction(playerAction).build();
		assertThat(turn.getPlayersActions(), hasSize(1));
		assertThat(turn.getPlayersActions(), hasItem(playerAction));
	}

	
}
