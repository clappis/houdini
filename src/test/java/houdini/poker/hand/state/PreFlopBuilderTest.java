package houdini.poker.hand.state;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import houdini.poker.action.Action;
import houdini.poker.player.Player;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PreFlopBuilderTest {

	@Mock
	private Map<Player, Action> playerAction;
	
	@Test
	public void buildPreFlop(){
		assertThat(new PreFlopBuilder().build(), instanceOf(PreFlop.class));
	}
	
	@Test
	public void buildPreFlopWithPlayerAction(){
		PreFlop preFlop = new PreFlopBuilder().addPlayerAction(playerAction).build();
		assertThat(preFlop.getPlayersActions(), hasSize(1));
		assertThat(preFlop.getPlayersActions(), hasItem(playerAction));
	}

	
}
