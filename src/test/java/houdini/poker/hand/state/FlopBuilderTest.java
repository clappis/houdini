package houdini.poker.hand.state;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import houdini.poker.action.Action;
import houdini.poker.card.Card;
import houdini.poker.player.Player;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FlopBuilderTest {

	@Mock
	private Card card;
	
	@Mock
	private Map<Player, Action> playerAction;
	
	@Test
	public void buildFlop(){
		assertThat(new FlopBuilder().build(), instanceOf(Flop.class));
	}
	
	@Test
	public void buildFlopWithFirstCard(){
		Flop flop = new FlopBuilder().firstCard(card).build();
		assertThat(flop.getFirstCard(), is(card));
	}
	
	@Test
	public void buildFlopWithSecondCard(){
		Flop flop = new FlopBuilder().secondCard(card).build();
		assertThat(flop.getSecondCard(), is(card));
	}
	
	@Test
	public void buildFlopWithThirdCard(){
		Flop flop = new FlopBuilder().thirdCard(card).build();
		assertThat(flop.getThirdCard(), is(card));
	}
	
	@Test
	public void buildFlopWithPlayerAction(){
		Flop flop = new FlopBuilder().addPlayerAction(playerAction).build();
		assertThat(flop.getPlayersActions(), hasSize(1));
		assertThat(flop.getPlayersActions(), hasItem(playerAction));
	}
	
}
