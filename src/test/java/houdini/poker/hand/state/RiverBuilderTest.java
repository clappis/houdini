package houdini.poker.hand.state;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import houdini.poker.action.Action;
import houdini.poker.card.Card;
import houdini.poker.player.Player;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RiverBuilderTest {

	@Mock
	private Map<Player, Action> playerAction;
	
	@Mock
	private Card card;
	
	@Test
	public void buildRiver(){
		assertThat(new RiverBuilder().build(), instanceOf(River.class));
	}
	
	@Test
	public void buildRiverWithCard(){
		River river = new RiverBuilder().card(card).build();
		assertThat(river.getCard(), is(card));
	}
	
	@Test
	public void buildRiverWithPlayerAction(){
		River river = new RiverBuilder().addPlayerAction(playerAction).build();
		assertThat(river.getPlayersActions(), hasSize(1));
		assertThat(river.getPlayersActions(), hasItem(playerAction));
	}

	
}
